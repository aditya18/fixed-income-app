"""fixed_income_software URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from fixed_income_app.seed_data_views import seed_data
from fixed_income_app.views import (
    render_reports,
    get_month_wise_reports,
    download_report,
    get_reports_namewise,
    download_name_wise_report,
    get_family_name_wise_reports,
    download_family_name_wise_reports,
    download_all_reports,
    health_check,
)
from django.views.generic.base import RedirectView
from django.conf.urls import url

urlpatterns = [
    # path("grappelli/", include("grappelli.urls")),
    url(r"^$", RedirectView.as_view(url="/admin/")),
    path("admin/", admin.site.urls),
    path("admin/view_reports/", render_reports),
    path("health_check/", health_check),
    path("admin/get_month_wise_report/", get_month_wise_reports),
    path("admin/download_report/<str:start_date>/<str:end_date>/", download_report),
    path("admin/get_name_wise_reports/", get_reports_namewise),
    path(
        "admin/download_report_namewise/<str:client_name>/", download_name_wise_report
    ),
    path("admin/get_family_name_wise_reports/", get_family_name_wise_reports),
    path(
        "admin/download_report_family_name_wise/<str:family_name>/",
        download_family_name_wise_reports,
    ),
    path("admin/download_all_reports/", download_all_reports),
    path("seed_data/", seed_data),
]


from django.conf.urls.i18n import i18n_patterns
from django.conf.urls import url

urlpatterns += [
    url(r"^i18n/", include("django.conf.urls.i18n")),
]
urlpatterns += i18n_patterns(url(r"^admin/", admin.site.urls))
