from django.test import TestCase
from fixed_income_app.models import ClientMaster
from datetime import datetime

class ClientMasterTestCase(TestCase):
    def set_up(self):
        for iter in range(0,20): 
            str = str(iter)
            ClientMaster.objects.create(
                entity = "Entity Name " + str,
                family_name = "Family Name " + str,
                member_name = "Member Name " + str,
                mobile_number = str * 10,
                email_id = "chat29aditya@gmail.com",
                address = "Addess " + str,
                pan_number = "Pan Number " + str, 
                principal = iter,
                investment_date = datetime.date(),
                maturity_date = datetime.date(),
                interest_rate = "Rate " + str,
                payment_frequency = "Payment Frequency " + str,
                cash_or_cheque_payment = "Cash or Cheque Payment " + str, 
                source_reference = "Source Reference " + str, 
                referral_fees = iter,
            )
            print("Created 0-> ")
