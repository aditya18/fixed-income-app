from django.core.exceptions import ValidationError
import re

def validate_phone_number_length(number):
    # if (len(number) < 10) or (len(number) > 10): 
    if len(number) != 10:
        raise ValidationError("#{number} is not a valid number".format(number=number))

def validate_email(email_id):
    if not re.search('^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$', email_id):
        raise ValidationError("#{email_id} is not a valid email Id".format(email_id=email_id))

def validate_payment_frequency_options(payment_frequency): 
    if payment_frequency.lower() not in ["monthly", "quarterly", "half yearly", "annual", "with maturity"]:
        raise ValidationError(
        "Invalid Payment Frequency Type. Possible options are 'Monthly', 'Quarterly', 'Half Yearly', 'Annula' or 'With Maturity'."
    )

def validate_image_type_by_content_type(pdc_copy): 
    content_type = pdc_copy.content_type.split("/")[0].strip.lower()
    if content_type != "image": 
        raise ValidationError(
        "Invalid content type for file of type image"
    )

def validate_if_valid_image_type(pdc_copy): 
    file_name = pdc_copy.file.name
    extension = file_name.split(".")[1].strip()
    if extension not in ["jpg", "png", "gif", "webp", "tiff", "psd", "raw", "bmp", "heif", "indd"]:
        raise ValidationError(
            "Invalid File Uploaded. Supported file types are jpg, png, gif, webp, tiff, psd, raw, bmp, heif, indd."
    )

    
def validate_if_pdf_or_word(agreement_copy):
    file_name = agreement_copy.file.name
    extension = file_name.split(".")[1].strip()
    if extension not in ["pdf", "doc", "docx", "dot"]:
        raise ValidationError(
        "Invalid file uploaded for agreement copy. Accepted file types are pdf, doc, docx, dot"
    )
