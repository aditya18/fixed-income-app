from django.contrib import admin
from django.db import models
from django.contrib.admin.widgets import AdminDateWidget

# Register your models here.
from .models import (
    ClientMaster,
    BankDetail,
    Transaction
)

class DontLog: 
    def log_addition(self, *args):
        return
    
    def log_change(self, *args):
        return

    def log_deletion(self, *args):
        return

class ClientMasterAdmin(DontLog, admin.ModelAdmin):
    fields = [
        "entity",
        "member_name", 
        "family_name",
        "mobile_number", 
        "email_id",
        "address",
        "pan_number",
        "principal", 
        "investment_date",
        "maturity_date",
        "interest_rate", 
        "original_interest_rate", 
        "payment_frequency",
        "payment",
        "cash_or_cheque_payment",
        "source_reference",
        "referral_fees",
        "agreement_copy",
        "pdc_copies",
        "remarks"
    ]
    list_display = ("member_name", "email_id", "mobile_number", "principal", "maturity_date", "interest_rate", "original_interest_rate", "family_name")
    list_filter = ("member_name", "email_id",)


class BankDetailAdmin(DontLog, admin.ModelAdmin): 
    list_display = ("id", "account_number", "account_type", "ifsc_code",)

class TransactionAdmin(DontLog, admin.ModelAdmin): 
    list_display = ("client", "in_cash_or_current_transaction", "final_closure",)

admin.site.register(ClientMaster, ClientMasterAdmin)
# admin.site.register(BankDetail, BankDetailAdmin)
admin.site.register(Transaction, TransactionAdmin)