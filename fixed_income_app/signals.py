from django.db.models import signals
from .email import send_email, send_client_master_email

def notify_about_transaction_changes(sender, instance, created, **kwargs):
    message = """\
    Subject: Changes to Transaction

    There were some changes made to the Transaction resource."""

    send_email(message)


def check_client_master_interest_rate(sender, instance, **kwargs): 
    interest_rate = instance.interest_rate
    if "%" in interest_rate: 
        splitted = interest_rate.split("%")[0].strip()
        instance.interest_rate = instance.principal * int(splitted)
        instance.original_interest_rate = interest_rate

def notify_about_client_master_changes(sender, instance, created, **kwargs):
    message = """\
        Subject: Changes to Client Master Data

        There were some changes made to the Client Master Resource. """
    send_client_master_email(message, instance.email_id)
    

# CHANGES TO BE TAKEN CARE OF 
# Transaction can be of 3 types
# Dropdown (Choice field)
# -> Interest ( No changes )
# -> Principal repaid ( If there is an amount, principal amount in client master would be reduced)
# -> Investment added (If there is any amount (More investment done), We will add in this case.)
# Implement a check such that the value in client master table can never be negative(less than 0). Give an error
# In reporting section, download reports of all the clientMaster records too. (View reports templates and download report for all the clients)
# Dropdown for specifying the format of report to be downloaded. (CSV and XLSX)