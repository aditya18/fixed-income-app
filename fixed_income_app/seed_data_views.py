from django.http import HttpResponse
from .utils import populate_client_master, populate_bank_details, populate_transactions

def seed_data(request): 
    populate_client_master()
    populate_bank_details()
    populate_transactions()
    return HttpResponse("Done.")