import smtplib, ssl
import os
from fixed_income_software import settings

def read_credentials_from_file():
    creds = []
    with open(os.getcwd() + "/email_credentials.txt", "r") as file:
        line = file.readline()
        creds = line.split(" ")
    return creds

MY_EMAIL, PASSWORD = read_credentials_from_file()
port = 465

def send_email(message):
    global MY_EMAIL, PASSWORD, port
    context = ssl.create_default_context()
    
    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server: 
        server.login(MY_EMAIL, PASSWORD)
        server.sendmail(MY_EMAIL, settings.RECEIVERS, message)

def send_client_master_email(message, email_id): 
    global MY_EMAIL, PASSWORD, port
    context = ssl.create_default_context()

    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server: 
        server.login(MY_EMAIL, PASSWORD)
        server.sendmail(MY_EMAIL, email_id, message)
