from django.shortcuts import render
from django.http import HttpResponse
from .models import ClientMaster, BankDetail, Transaction
from .forms import DateRangeForm
from django.db.models import Sum
from .utils import (
    get_formatted_transactions,
    get_formatted_date,
    get_formatted_date_for_report,
    get_all_client_names,
    get_transactions_by_customer_name,
    get_all_family_names,
    get_transactions_by_family_name,
    get_columns_names_for_csv,
    get_file_type_response_object,
    get_formatted_clients,
    get_past_and_future_transactions,
)
import csv

# Create your views here.


def get_formatted_interests(all_interests):
    formatted = []
    for interest in all_interests:
        formatted.append(int(interest[0]) * int(interest[1]))
    #     if "%" in interest:
    #         interest = int(interest.replace("%", "").strip())
    #         formatted.append(interest)
    #     else:
    #         formatted.append(int(interest))
    # print(formatted)
    return sum(formatted)


def render_reports(request):
    total_clients = ClientMaster.objects.all().count()
    total_principal = ClientMaster.objects.aggregate((Sum("principal")))[
        "principal__sum"
    ]
    all_interests = ClientMaster.objects.all().values_list("interest_rate", "principal")
    all_interests = get_formatted_interests(all_interests)
    date_range_form = DateRangeForm()
    client_names = get_all_client_names()
    family_names = get_all_family_names()
    return render(
        request,
        "reports.html",
        {
            "total_clients": total_clients,
            "all_interests": all_interests,
            "total_principal": total_principal,
            "form": date_range_form,
            "client_names": client_names,
            "family_names": family_names,
        },
    )


def get_month_wise_reports(request):
    form = DateRangeForm(request.POST)
    if form.is_valid():
        start_date, end_date = [
            form.cleaned_data["start_date"],
            form.cleaned_data["end_date"],
        ]
        if end_date < start_date:
            return HttpResponse(
                "End Date cannot be less than start date",
                status_code=400,
                content_type="application/json",
            )
        formatted_transactions = get_past_and_future_transactions(start_date, end_date)
        total_amount = sum(
            [transaction["amount"] for transaction in formatted_transactions]
        )
        return render(
            request,
            "date_wise_report.html",
            {
                "transactions": formatted_transactions,
                "start_date": start_date,
                "end_date": end_date,
                "total_amount": total_amount,
            },
        )
        # return render(request, "report_view.html",
        # {
        # "transactions": formatted_transactions,
        # "start_date": start_date,
        # "end_date": end_date,
        # })

    else:
        return HttpResponse("Error. Please try again!")


def download_report(request, start_date, end_date):
    download_format = request.POST["download_format"]
    response = get_file_type_response_object(download_format)
    start_date = get_formatted_date_for_report(start_date)
    end_date = get_formatted_date_for_report(end_date)
    formatted_transactions = get_formatted_transactions(start_date, end_date)
    try:
        file = open("report.csv", "w")
        writer = csv.writer(response)
        writer.writerow(get_columns_names_for_csv())
        for data in formatted_transactions:
            writer.writerow(data.values())
        return response
    except Exception as e:
        print("EXCEPTION -> ", e)
        return HttpResponse("There was an error while downloading the report ! :(")


def download_name_wise_report(request, client_name):
    download_format = request.POST["download_format"]
    response = get_file_type_response_object(download_format)
    formatted_transactions = get_transactions_by_customer_name(client_name)
    try:
        file = open("report.csv", "w")
        writer = csv.writer(response)
        writer.writerow(get_columns_names_for_csv())
        for data in formatted_transactions:
            writer.writerow(data.values())
        return response
    except Exception as e:
        print("EXCEPTION -> ", e)
        return HttpResponse("There was an error while downloading the report ! :(")


def download_family_name_wise_reports(request, family_name):
    download_format = request.POST["download_format"]
    response = get_file_type_response_object(download_format)
    formatted_transactions = get_transactions_by_family_name(family_name)
    try:
        file = open("report.csv", "w")
        writer = csv.writer(response)
        writer.writerow(get_columns_names_for_csv())
        for data in formatted_transactions:
            writer.writerow(data.values())
        return response
    except Exception as e:
        print("Exception -> ", e)
        return HttpResponse("There was an error while downloading the report ! :(")


def download_all_reports(request):
    download_format = request.POST["download_format"]
    file_type_response = get_file_type_response_object(download_format)
    all_clients = get_formatted_clients()
    try:
        writer = csv.writer(file_type_response)
        writer.writerow(get_columns_names_for_csv())
        for data in all_clients:
            writer.writerow(data.values())
        return file_type_response
    except Exception as e:
        print("Exception -> ", e)
        return HttpResponse("There was an error while downloading the report ! :(")


def get_reports_namewise(request):
    customer_name = request.POST["customer_type"]
    formatted_transactions = get_transactions_by_customer_name(customer_name)
    return render(
        request,
        "name_wise_report.html",
        {
            "transactions": formatted_transactions,
            "client_name": customer_name,
        },
    )


def get_family_name_wise_reports(request):
    family_name = request.POST["family_name"]
    return render(
        request,
        "family_name_wise_report.html",
        {
            "transactions": get_transactions_by_family_name(family_name),
            "family_name": family_name,
        },
    )
    return HttpResponse("Ok")


def health_check(request):
    return HttpResponse("Ok")