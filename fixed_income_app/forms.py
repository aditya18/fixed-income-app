from django import forms
from functools import partial
from datetime import datetime

DateInput = partial(forms.DateInput, {'class': 'datepicker', 'value': datetime.now().strftime("%m/%d/%Y")})

class DateRangeForm(forms.Form):
    start_date = forms.DateField(widget=DateInput())
    end_date = forms.DateField(widget=DateInput(), required=False)
