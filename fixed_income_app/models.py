from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ValidationError
import uuid
from .model_validations import (
    validate_phone_number_length,
    validate_email,
    validate_payment_frequency_options,
    validate_if_pdf_or_word,
    validate_if_valid_image_type,
)
from fixed_income_software import settings
from django.db.models import signals
from .signals import (
    notify_about_client_master_changes,
    check_client_master_interest_rate,
    notify_about_transaction_changes,
)

# Create your models here.


class ClientMaster(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    entity = models.CharField(
        max_length=30,
        choices=[("Scale Up", "Scale Up"), ("H-Zone", "H-Zone")],
        default="Scale Up",
        null=True,
        blank=True,
    )
    family_name = models.CharField(max_length=30, null=True, blank=True)
    member_name = models.CharField(
        max_length=30, null=True, blank=True, verbose_name="Investor"
    )
    mobile_number = models.CharField(max_length=20, null=True, blank=True)
    email_id = models.CharField(max_length=60, null=True, blank=True)
    address = models.TextField(max_length=500, null=True, blank=True)
    pan_number = models.CharField(max_length=20, null=True, blank=True)
    principal = models.IntegerField(default=0, null=True, blank=True)
    investment_date = models.DateField(null=True, blank=True)
    maturity_date = models.DateField(null=True, blank=True)
    interest_rate = models.CharField(max_length=20, null=True, blank=True)
    original_interest_rate = models.CharField(
        max_length=20, blank=True, null=True, editable=True
    )
    payment_frequency = models.CharField(
        max_length=20,
        choices=[
            ("Monthly", "Monthly"),
            ("Quarterly", "Quarterly"),
            ("Half Yearly", "Half Yearly"),
            ("Annual", "Annual"),
            ("With Maturity", "With Maturity"),
        ],
        default="Monthly",
        null=True,
        blank=True,
    )
    payment = models.IntegerField(default=0, null=True, blank=True)
    cash_or_cheque_payment = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        choices=[
            ("Cash", "Cash"),
            ("Cheque", "Cheque"),
        ],
        verbose_name="Cash/Bank",
    )
    source_reference = models.CharField(max_length=30, null=True, blank=True)
    referral_fees = models.IntegerField(default=0, null=True, blank=True)
    agreement_copy = models.FileField(
        null=True, blank=True, upload_to="media", validators=[validate_if_pdf_or_word]
    )
    pdc_copies = models.FileField(
        null=True,
        blank=True,
        upload_to="media",
        validators=[validate_if_valid_image_type],
    )
    remarks = models.TextField(max_length=500, blank=True, null=True)
    inserted_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.member_name


class BankDetail(models.Model):
    id = models.OneToOneField(ClientMaster, primary_key=True, on_delete=models.CASCADE)
    account_number = models.CharField(max_length=20)
    account_type = models.CharField(max_length=20)
    ifsc_code = models.CharField(max_length=20)
    inserted_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)


class Transaction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    client = models.ForeignKey(ClientMaster, on_delete=models.CASCADE)
    date = models.DateField()
    in_cash_or_current_transaction = models.CharField(max_length=20)
    transaction_type = models.CharField(
        max_length=30,
        choices=[
            ("Interest", "Interest"),
            ("Principal Repaid", "Principal Repaid"),
            ("Investment Added", "Investment Added"),
        ],
        default="Interest",
    )
    amount = models.IntegerField(default=0)
    # amount = models.MoneyField(
    #     max_digits=30,
    #     decimal_places=2,
    #     default_currency="INR"
    # )
    final_closure = models.BooleanField(default=False)
    inserted_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)


def get_client_by_client_id(client_id):
    return ClientMaster.objects.get(id=client_id)


def get_updated_interest_rate(original):
    return (
        int(original.split(" ")[0].strip())
        if ("%" in original)
        else int(original.strip())
    )


def add_principal_in_client_master(transaction_instance):
    amount = transaction_instance.amount
    client = get_client_by_client_id(transaction_instance.client.id)
    updated_principal = client.principal + amount
    interest_rate = client.interest_rate
    updated_interest_rate = updated_principal * get_updated_interest_rate(
        client.original_interest_rate
    )
    if updated_principal < 0:
        raise ValidationError(
            "The principal Value in ClientMaster resource cannot be less than 0"
        )
    else:
        client.principal = updated_principal
        client.interest_rate = str(updated_interest_rate)
        client.save()


def reduce_principal_in_clientmaster(transaction_instance):
    amount = transaction_instance.amount
    client = get_client_by_client_id(transaction_instance.client.id)
    updated_principal = client.principal - amount
    interest_rate = client.interest_rate
    updated_interest_rate = updated_principal * get_updated_interest_rate(
        client.original_interest_rate
    )
    if updated_principal < 0:
        raise ValidationError(
            "The principal Value in ClientMaster resource cannot be less than 0"
        )
    else:
        client.principal = updated_principal
        client.interest_rate = str(updated_interest_rate)
        client.save()


def update_principal_as_per_transaction_type(sender, instance, created, **kwargs):
    transaction_type = instance.transaction_type
    if transaction_type == "Principal Repaid":
        reduce_principal_in_clientmaster(instance)
    elif transaction_type == "Investment Added":
        add_principal_in_client_master(instance)
    else:
        pass


# SIGNALS

# signals.post_save.connect(receiver=notify_about_transaction_changes, sender=Transaction)
signals.pre_save.connect(
    receiver=check_client_master_interest_rate, sender=ClientMaster
)
# signals.post_save.connect(receiver=notify_about_client_master_changes, sender=ClientMaster)
signals.post_save.connect(
    receiver=update_principal_as_per_transaction_type, sender=Transaction
)


# principal times annual interest rate for total interst

# total sum amount on the web page itself

# UI changes

# Amount in date range which would be payment column (which is thecolumn next to payment frequency)