# Generated by Django 3.1.4 on 2021-01-11 06:00

from django.db import migrations, models
import fixed_income_app.model_validations


class Migration(migrations.Migration):

    dependencies = [
        ('fixed_income_app', '0026_auto_20210107_0851'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transaction',
            name='amount',
        ),
        migrations.AlterField(
            model_name='clientmaster',
            name='agreement_copy',
            field=models.FileField(blank=True, null=True, upload_to='C:\\code\\Fixed_income_software\\media/', validators=[fixed_income_app.model_validations.validate_if_pdf_or_word]),
        ),
        migrations.AlterField(
            model_name='clientmaster',
            name='member_name',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Investor'),
        ),
        migrations.AlterField(
            model_name='clientmaster',
            name='pdc_copies',
            field=models.FileField(blank=True, null=True, upload_to='C:\\code\\Fixed_income_software\\media/', validators=[fixed_income_app.model_validations.validate_if_valid_image_type]),
        ),
    ]
