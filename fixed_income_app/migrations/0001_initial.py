# Generated by Django 3.1.2 on 2020-12-10 16:26

import django.contrib.postgres.fields
from django.db import migrations, models
import fixed_income_app.model_validations
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bank_Details',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('account_number', models.CharField(max_length=20)),
                ('account_type', models.CharField(max_length=20)),
                ('ifsc_code', models.CharField(max_length=20)),
                ('inserted_at', models.DateTimeField(auto_now=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Client_Master',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('entity', models.CharField(max_length=30)),
                ('family_name', models.CharField(max_length=30)),
                ('member_name', models.CharField(max_length=30)),
                ('mobile_number', models.CharField(max_length=10, validators=[fixed_income_app.model_validations.validate_phone_number_length])),
                ('email_id', models.CharField(max_length=30, validators=[fixed_income_app.model_validations.validate_email])),
                ('address', models.CharField(max_length=50)),
                ('pan_number', models.CharField(max_length=20)),
                ('principal', models.IntegerField(default=0)),
                ('investment_date', models.DateTimeField(auto_now=True)),
                ('maturity_date', models.DateTimeField(auto_now=True)),
                ('interest_rate', models.CharField(max_length=20)),
                ('payment_frequency', models.CharField(max_length=20, validators=[fixed_income_app.model_validations.validate_payment_frequency_options])),
                ('cash_or_cheque_payment', models.CharField(max_length=20)),
                ('bank_details_id', models.UUIDField()),
                ('source_reference', models.CharField(max_length=30)),
                ('referral_fees', models.IntegerField(default=0)),
                ('agreement_copy_path', models.CharField(max_length=200)),
                ('pdc_copies', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=200), size=100)),
                ('remarks', models.CharField(max_length=500)),
                ('inserted_at', models.DateTimeField(auto_now=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Transactions',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('client_id', models.UUIDField()),
                ('date', models.DateTimeField(auto_now=True)),
                ('paid_interest', models.IntegerField(default=0)),
                ('in_cash_or_current_transaction', models.CharField(max_length=20)),
                ('principal_repaid', models.IntegerField(default=0)),
                ('final_closure', models.BooleanField(default=False)),
                ('inserted_at', models.DateTimeField(auto_now=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
