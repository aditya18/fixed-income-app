# Generated by Django 3.1.4 on 2021-01-11 06:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fixed_income_app', '0027_auto_20210111_1130'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='amount',
            field=models.IntegerField(default=0),
        ),
    ]
