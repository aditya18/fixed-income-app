from django.apps import AppConfig


class FixedincomeappConfig(AppConfig):
    name = 'fixed_income_app'
