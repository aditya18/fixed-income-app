from .models import ClientMaster, Transaction
import calendar
from datetime import datetime, timedelta
import names
import itertools
import random
from django.http import HttpResponse
from dateutil.relativedelta import relativedelta
from dateutil.rrule import *

# name 
# email id 
# contact details
# principal 
# interest rate

def convert_clients_to_dicts(client_masters):
    formatted = [
        {
            "member_name":  client.member_name,
            "family_name": client.family_name,
            "mobile_number": client.mobile_number,
            "email_id": client.email_id,
            "principal": client.principal,
            "interest_rate": client.interest_rate
        }
        for client in client_masters
    ]
    return formatted

def convert_transaction_object_to_dict(transactions):
    formatted = [
        {
            "client_name": t.client.member_name,
            "family_name": t.client.family_name,
            "entity": t.client.entity,
            "mobile_number": t.client.mobile_number,
            "email_address": t.client.email_id,
            "total_principal": t.client.principal,
            "interest_rate": t.client.original_interest_rate, 
            "interest_amount": t.client.interest_rate, 
            "maturity_date": get_formatted_date(t.client.maturity_date),
            "referral_name": t.client.source_reference
        }
        for t in transactions
    ]
    return formatted

def populate_transactions(): 
    all_clients = list(ClientMaster.objects.all())
    now = datetime.now()
    possible_transaction_types = [
        "Interest",
        "Principal Repaid",
        "Investment Added"
    ]
    today = datetime(now.year, now.month, now.day)
    for iter in range(0, 50): 
        Transaction.objects.create(
            client = random.choice(all_clients),
            date = today,
            amount=iter,
            in_cash_or_current_transaction = random.choice(["In cash", "Current"]),
            transaction_type ="Interest",
            final_closure = random.choice([True, False]),
            inserted_at = datetime.now()
        )

def populate_client_master(): 
    def get_updated_mobile_number(number):
        if len(number) == 1:
            return number * 10
        elif len(number) == 2: 
            return number * 5

    for iter in range(0, 30): 
        entity_choices = ["Scale Up", "H-Zone"]
        payment_frequencies = ["With Maturity", "Annual", "Half Yearly", "Quarterly", "Monthly"]
        payment_choices = ["Cash", "Cheque"]
        full_name = names.get_full_name()
        splitted_full_name = full_name.split(" ")
        converted = str(iter)
        now = datetime.now()
        today = datetime(now.year, now.month, now.day)
        clientmaster_object = ClientMaster(
            entity = entity_choices[random.randint(0, 1)],
            member_name = full_name,
            family_name = full_name + "'s " + "Family",
            mobile_number = get_updated_mobile_number(str(iter)),
            email_id = splitted_full_name[0] + "_" + splitted_full_name[1] + converted+ "@gmail.com",
            address = "Addess " + converted,
            pan_number = "Pan Number " + converted, 
            principal = iter,
            investment_date = today,
            maturity_date = today,
            interest_rate = str(25 * iter),
            original_interest_rate = "25 %",
            payment_frequency = payment_frequencies[random.randint(0, 4)],
            payment = 10000,
            cash_or_cheque_payment = payment_choices[random.randint(0, 1)],
            source_reference = "Source Reference " + converted, 
            referral_fees = iter * 10,
            inserted_at = datetime.now()
        )
        clientmaster_object.save()

def populate_bank_details(): 
    pass

def get_formatted_date(date_object):
    return "/".join([str(date_object.month), str(date_object.day), str(date_object.year)])

def get_formatted_transactions(start_date, end_date): 
    end_date = datetime(end_date.year, end_date.month, end_date.day)
    end_date += timedelta(days=1)
    transactions = Transaction.objects.filter(inserted_at__range=(start_date, end_date))
    formatted = convert_transaction_object_to_dict(transactions)
    return formatted

def get_month(month):
    month_dict = {
        "Jan": "01", 
        "Feb": "02", 
        "Mar": "03",
        "Apr": "04",
        "May": "05",
        "Jun": "06", 
        "Jul": "07", 
        "Aug": "08",
        "Sep": "09",
        "Oct": "10",
        "Nov": "11", 
        "Dec": "12"
    }
    return month_dict[month]

def get_formatted_date_for_report(date): 
    splitted = date.split(",")
    year = int(splitted[1])
    day = int(splitted[0].split(".")[1].strip())
    month = int(get_month(splitted[0].split(".")[0]))
    return datetime(year, month, day)

def get_all_clients(): return ClientMaster.objects.all()

def get_all_client_names():
    return [client.member_name for client in get_all_clients() ]

def get_all_family_names(): 
    return [client.family_name for client in get_all_clients()]

def get_transactions_by_customer_name(client_name): 
    get_client_by_client_name = lambda client_name: ClientMaster.objects.get(member_name=client_name) 
    transactions = Transaction.objects.filter(client=get_client_by_client_name(client_name))
    formatted = convert_transaction_object_to_dict(transactions)
    return formatted

def get_transactions_by_family_name(family_name): 
    get_client_by_family_name = lambda family_name: ClientMaster.objects.get(family_name=family_name)
    transactions = Transaction.objects.filter(client=get_client_by_family_name(family_name))
    formatted = convert_transaction_object_to_dict(transactions)
    return formatted

def get_columns_names_for_csv():
    return  [
        "Client Name", 
        "Paid Interest", 
        "In Cash or Current Transaction",
        "Principal Repaid",
        "Final Closure", 
        "Date"
    ]

def get_file_type_response_object(download_format):
    response = HttpResponse(content_type="text/csv")
    filename = "reports." + download_format.lower()
    response["Content-Disposition"] = "attachment; filename=" + filename
    return response

def get_formatted_clients(): 
    all_clients = ClientMaster.objects.all()
    formatted = convert_clients_to_dicts(all_clients)
    return formatted

def get_future_clients(end_date):
    clients = ClientMaster.objects.all()
    clients = list(map(lambda queryset: vars(queryset), clients))
    return clients

def calculate_number_of_transactions_till_maturity(client, end_date): 
    diff = client["maturity_date"] - end_date
    return diff.days // 30

def calculate_count_for_quarterly(client, end_date): 
    diff = client["maturity_date"] - end_date
    return diff.days // 90

def remove_none_vals(l):
    return [
        val for val in l if val is not None
    ]

def get_transactions(client, start_date, end_date, transaction_dates): 
    future_dates = []
    today = datetime.today().date()
    get_client_obj = lambda uuid: ClientMaster.objects.get(id=uuid)
    past_transactions = [
        {
            "client_name": client["member_name"],
            "payment_frequency": client["payment_frequency"],
            "transaction_date": get_formatted_date(t.date),
            "transaction_type": t.transaction_type,
            "amount": client["payment"],
            "final_closure": t.final_closure
        }
        for t in Transaction.objects.filter(inserted_at__range=(start_date, today), client=get_client_obj(client["id"]))
    ]
    for date in transaction_dates:
        if today <  date < end_date:
            future_dates.append(date)
        else:
            pass
    future_transactions = [
        {
            "client_name": client["member_name"],
            "payment_frequency": client["payment_frequency"],
            "transaction_date": get_formatted_date(date),
            "transaction_type": "Interest",
            "amount" : client["payment"],
            "final_closure": "False"
        }
        for date in future_dates
    ]
    return (past_transactions + future_transactions)

def get_client_with_maturity(client, start_date, end_date): 
    maturity_date = client["maturity_date"]
    get_client_obj = lambda uuid: ClientMaster.objects.get(id=uuid)
    today = datetime.today().date()
    if start_date <= maturity_date <= today: 
        maturity_date = datetime(maturity_date.year, maturity_date.month, maturity_date.day)
        maturity_date += timedelta(days=1)
        return [
            {
                "client_name": client["member_name"],
                "payment_frequency": client["payment_frequency"],
                "transaction_date": get_formatted_date(t.date),
                "transaction_type": t.transaction_type,
                "amount": client["payment"],
                "final_closure": t.final_closure
            }
            for t in Transaction.objects.filter(inserted_at__range = (start_date, maturity_date), client=get_client_obj(client["id"]))
        ]
    elif today < maturity_date <= end_date:
        return [
            {
                "client_name": client["member_name"],
                "payment_frequency": client["payment_frequency"],
                "transaction_date": get_formatted_date(maturity_date),
                "transaction_type": "Interest",
                "amount" : client["payment"],
                "final_closure": "False"
            }
        ]

def get_all_transactions_for_client(client, start_date, end_date):
    investment_date = client["investment_date"]
    maturity_date = client["maturity_date"]
    if client["payment_frequency"] == "Monthly": 
        number_of_days =  [dt.date() for dt in rrule(MONTHLY, dtstart=investment_date, until=maturity_date)]
        return get_transactions(client, start_date, end_date, number_of_days)

    elif client["payment_frequency"] == "Annual":
        number_of_days = [dt.date() for dt in rrule(YEARLY, dtstart=investment_date, until=maturity_date)]
        return get_transactions(client, start_date, end_date, number_of_days)

    elif client["payment_frequency"] == "Quarterly":
        quarters = [dt.date() for dt in rrule(MONTHLY, interval=3, dtstart=investment_date, until=maturity_date)]
        return get_transactions(client, start_date, end_date, quarters)

    elif client["payment_frequency"] == "Half Yearly":
        half_years = [dt.date() for dt in rrule(MONTHLY, interval=6, dtstart=investment_date, until=maturity_date)]
        return get_transactions(client, start_date, end_date, half_years)

    elif client["payment_frequency"] == "With Maturity":
        report = get_client_with_maturity(client, start_date, end_date)
        return report


def get_past_and_future_transactions(start_date, end_date):
    end_date = datetime(end_date.year, end_date.month, end_date.day).date()
    start_date = datetime(start_date.year, start_date.month, start_date.day).date()
    future_clients = get_future_clients(end_date)
    formatted_reports = []
    for client in future_clients: 
        formatted_reports.append(get_all_transactions_for_client(client, start_date, end_date))
    return list(itertools.chain(*remove_none_vals(formatted_reports)))
            